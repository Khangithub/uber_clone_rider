class CustomedUser {
  String fullName;
  String email;
  String phone;
  String uid;

  CustomedUser({this.email, this.fullName, this.phone, this.uid});

  setUser({String uuid, String ffullname, String pphone, String eemail}) {
    uid = uuid;
    phone = pphone;
    email = eemail;
    fullName = ffullname;
  }
}
