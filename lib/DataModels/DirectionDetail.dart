class DirectionDetail {
  String distanceText;
  String durationText;
  int distanceValue;
  int durationValue;
  String encodedPoints;

  DirectionDetail(
      {this.distanceText,
      this.durationText,
      this.distanceValue,
      this.durationValue,
      this.encodedPoints});
}
