import 'dart:math';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:uber_clone/DataModels/Address.dart';
import 'package:uber_clone/DataModels/DirectionDetail.dart';
import 'package:uber_clone/Helpers/RequestHepers.dart';
import 'package:uber_clone/Providers/AppData.dart';

import '../GlobalVariables.dart';

class HelperMethods {
  static void getCurrentUserInfo() async {
    print('getting data of user');
    var currentFirebaseUser = FirebaseAuth.instance.currentUser;
    String userId = currentFirebaseUser.uid;

    DatabaseReference useRef =
        FirebaseDatabase.instance.reference().child('user/$userId');

    useRef.once().then((snapshot) {
      if (snapshot.value != null) {
        currentUserInfo.setUser(
            uuid: snapshot.key,
            pphone: snapshot.value['phone'],
            ffullname: snapshot.value['fullname'],
            eemail: snapshot.value['email']);

        print('chuc mung Tam nha ${currentUserInfo.fullName}');
      }
    });
  }

  static Future<String> findCordinateAddress(Position position, context) async {
    String placeAddress = 'empty string';
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult != ConnectivityResult.mobile &&
        connectivityResult != ConnectivityResult.wifi) {
      return placeAddress;
    }

    String url =
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=$mapAPIKey';

    var response = await RequestHelpers.getRequest(url);

    if (response != 'failed') {
      placeAddress =
          response['results'][0]['address_components'][0]['long_name'];
      Address pickupAddress = new Address(
          latitude: position.latitude,
          longitude: position.longitude,
          placeName: placeAddress);

      Provider.of<AppData>(context, listen: false)
          .updatePickupAddress(pickupAddress);
    }

    return placeAddress;
  }

  static Future<DirectionDetail> getDirectionDetails(
      LatLng startPosition, LatLng endPosition) async {
    String url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=${startPosition.latitude},${startPosition.longitude}&destination=${endPosition.latitude},${endPosition.longitude}&mode=driving&key=$mapAPIKey';
    var response = await RequestHelpers.getRequest(url);

    if (response == 'failed') return null;

    DirectionDetail directionDetail = new DirectionDetail(
        durationText: response['routes'][0]['legs'][0]['duration']['text'],
        durationValue: response['routes'][0]['legs'][0]['duration']['value'],
        distanceText: response['routes'][0]['legs'][0]['distance']['text'],
        distanceValue: response['routes'][0]['legs'][0]['distance']['value'],
        encodedPoints: response['routes'][0]['overview_polyline']['points']);
    return directionDetail;
  }

  static int estimateFares(DirectionDetail details) {
    // per km = $0.3
    // per minute = $0.2
    // base fare = $3

    double baseFare = 3;
    double distanceFare = (details.distanceValue / 1000) * 0.3;
    double timeFare = (details.durationValue / 60) * 0.2;

    return (baseFare + distanceFare + timeFare).truncate();
  }

  static double generateRandomNumer(int max) {
    var randomGenerator = Random();
    int randInt = randomGenerator.nextInt(max);

    return randInt.toDouble();
  }
}
