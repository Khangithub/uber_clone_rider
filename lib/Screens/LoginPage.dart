import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:uber_clone/BrandColors.dart';
import 'package:uber_clone/Screens/MainPage.dart';
import 'package:uber_clone/Screens/RegistrationPage.dart';
import 'package:uber_clone/Widgets/TaxiButton.dart';

class LoginPage extends StatelessWidget {
  static const String id = 'login';

  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  String getInputError() {
    if (!emailController.text.contains('@')) {
      return 'Please enter valid email address';
    }

    if (passwordController.text.length < 8) {
      return 'Your password is too short, please enter another one which has more than 8 characters length';
    }
    return null;
  }

  void loginUser(BuildContext context) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: emailController.text, password: passwordController.text);

      if (userCredential != null) {
        Navigator.pushNamedAndRemoveUntil(
            context, MainPage.id, (route) => false);
      }
    } on FirebaseAuthException catch (e) {
      var error;
      Navigator.pop(context);
      if (e.code == 'user-not-found') {
        error = 'No user found for that email';
      }

      if (e.code == 'wrong-password') {
        error = 'Wrong password provided for that user';
      }

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(error),
        duration: const Duration(seconds: 1),
        action: SnackBarAction(
          label: 'close',
          onPressed: () {},
        ),
      ));
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100.0,
              ),
              Image(
                alignment: Alignment.center,
                height: 100.0,
                width: 100.0,
                image: AssetImage('images/logo.png'),
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                'Sign In as a Rider',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 25, fontFamily: 'Brand-Bold'),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    TextField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          labelText: 'Email Address',
                          labelStyle: TextStyle(fontSize: 14.0),
                          hintStyle:
                              TextStyle(color: Colors.grey, fontSize: 10.0)),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                          labelText: 'Password',
                          labelStyle: TextStyle(fontSize: 14.0),
                          hintStyle:
                              TextStyle(color: Colors.grey, fontSize: 10.0)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TaxiButton(
                      title: 'Log in',
                      color: BrandColors.colorAccent,
                      onPressed: () {
                        if (getInputError() != null) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(getInputError()),
                            duration: const Duration(seconds: 1),
                            action: SnackBarAction(
                              label: 'close',
                              onPressed: () {},
                            ),
                          ));
                          return;
                        }
                        loginUser(context);
                      },
                    )
                  ],
                ),
              ),
              TextButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, RegistrationPage.id, (route) => false);
                  },
                  child: Text('Don\'t have an account, sign up here'))
            ],
          ),
        ),
      ),
    );
  }
}
