import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:uber_clone/BrandColors.dart';
import 'package:uber_clone/Screens/LoginPage.dart';
import 'package:uber_clone/Screens/MainPage.dart';
import 'package:uber_clone/Widgets/TaxiButton.dart';

// ignore: must_be_immutable
class RegistrationPage extends StatelessWidget {
  static const String id = 'register';

  final FirebaseAuth _auth = FirebaseAuth.instance;

  var fullnameController = TextEditingController();
  var phoneController = TextEditingController();
  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  String getInputError() {
    if (fullnameController.text.length < 3) {
      return 'Please enter valid full name';
    }

    if (!emailController.text.contains('@')) {
      return 'Please enter valid email address';
    }

    if (phoneController.text.length < 10) {
      return 'Please enter valid phone number';
    }

    if (passwordController.text.length < 8) {
      return 'Your password is too short, please enter another one which has more than 8 characters length';
    }
    return null;
  }

  void registerUser(BuildContext context) async {
    final User user = (await _auth.createUserWithEmailAndPassword(
            email: emailController.text, password: passwordController.text))
        .user;

    if (user != null) {
      DatabaseReference newUserRef =
          FirebaseDatabase.instance.reference().child('user/${user.uid}');

      Map userMap = {
        'fullname': fullnameController.text,
        'email': emailController.text,
        'phone': phoneController.text
      };

      newUserRef.set(userMap);
      Navigator.pushNamedAndRemoveUntil(context, MainPage.id, (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100.0,
              ),
              Image(
                alignment: Alignment.center,
                height: 100.0,
                width: 100.0,
                image: AssetImage('images/logo.png'),
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                'Register as a Rider',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 25, fontFamily: 'Brand-Bold'),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    TextField(
                      controller: fullnameController,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                          labelText: 'Full Name',
                          labelStyle: TextStyle(fontSize: 14.0),
                          hintStyle:
                              TextStyle(color: Colors.grey, fontSize: 10.0)),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          labelText: 'Email',
                          labelStyle: TextStyle(fontSize: 14.0),
                          hintStyle:
                              TextStyle(color: Colors.grey, fontSize: 10.0)),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextField(
                      controller: phoneController,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          labelText: 'Phone Number',
                          labelStyle: TextStyle(fontSize: 14.0),
                          hintStyle:
                              TextStyle(color: Colors.grey, fontSize: 10.0)),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                          labelText: 'Password',
                          labelStyle: TextStyle(fontSize: 14.0),
                          hintStyle:
                              TextStyle(color: Colors.grey, fontSize: 10.0)),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TaxiButton(
                      title: 'Register',
                      color: BrandColors.colorAccent,
                      onPressed: () {
                        if (getInputError() != null) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(getInputError()),
                            duration: const Duration(seconds: 1),
                            action: SnackBarAction(
                              label: 'close',
                              onPressed: () {},
                            ),
                          ));
                          return;
                        }
                        registerUser(context);
                      },
                    )
                  ],
                ),
              ),
              TextButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, LoginPage.id, (route) => false);
                  },
                  child: Text('Already have RIDER account? login here'))
            ],
          ),
        ),
      ),
    );
    ;
  }
}
