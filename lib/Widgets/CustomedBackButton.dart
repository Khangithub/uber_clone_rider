import 'package:flutter/material.dart';
import 'package:uber_clone/BrandColors.dart';

class CustomedBackButton extends StatelessWidget {
  final String text;
  final Function onTap;

  CustomedBackButton({this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25),
              border: Border.all(
                  width: 1.0, color: BrandColors.colorLightGrayFair)),
          child: GestureDetector(
            onTap: onTap,
            child: Icon(
              Icons.close,
              size: 25,
            ),
          ),
        ),
        SizedBox(
          height: 15,
        ),
        Text(text)
      ],
    );
  }
}
