import 'package:flutter/material.dart';

class DrawerListItem extends StatelessWidget {
  final String title;
  final IconData icon;

  DrawerListItem({this.title, this.icon});

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Icon(icon),
        title: Text(
          title,
          style: TextStyle(fontSize: 17),
        ));
  }
}
