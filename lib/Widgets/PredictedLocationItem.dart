import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uber_clone/BrandColors.dart';
import 'package:uber_clone/DataModels/Address.dart';
import 'package:uber_clone/DataModels/Prediction.dart';
import 'package:uber_clone/GlobalVariables.dart';
import 'package:uber_clone/Helpers/RequestHepers.dart';
import 'package:uber_clone/Providers/AppData.dart';

class PredictedLocationItem extends StatelessWidget {
  final Prediction prediction;

  PredictedLocationItem({this.prediction});

  void getPlaceDetail(String placeID, BuildContext context) async {
    String url =
        'https://maps.googleapis.com/maps/api/place/details/json?placeid=$placeID&key=$mapAPIKey';
    var response = await RequestHelpers.getRequest(url);

    if (response == 'failed') return;

    if (response['status'] == 'OK') {
      Address thisPlace = Address(
          placeName: response['result']['name'],
          placeId: placeID,
          latitude: response['result']['geometry']['location']['lat'],
          longitude: response['result']['geometry']['location']['lng']);

      Provider.of<AppData>(context, listen: false)
          .updateDestinationAddress(thisPlace);

      print('this place ${thisPlace.placeName}');

      Navigator.pop(context, 'getDirection');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: () {
        getPlaceDetail(prediction.placeId, context);
      },
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.location_on,
              color: BrandColors.colorDimText,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    prediction.mainText,
                    style: TextStyle(fontSize: 16),
                  ),
                  Text(
                    prediction.secondaryText,
                    style: TextStyle(
                        fontSize: 12, color: BrandColors.colorDimText),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
