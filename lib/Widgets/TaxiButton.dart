import 'package:flutter/material.dart';

class TaxiButton extends StatelessWidget {
  final String title;
  final Color color;
  final Function onPressed;

  TaxiButton(
      {@required this.title, @required this.color, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          primary: color,
          onPrimary: color,
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
        ),
        child: Container(
          width: double.infinity,
          height: 50,
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                  fontFamily: 'Brand-Bold', color: Colors.white, fontSize: 30),
            ),
          ),
        ));
  }
}
