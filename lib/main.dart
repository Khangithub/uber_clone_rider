import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uber_clone/Providers/AppData.dart';
import 'package:uber_clone/Screens/LoginPage.dart';
import 'package:uber_clone/Screens/MainPage.dart';
import 'package:uber_clone/Screens/RegistrationPage.dart';
import 'package:uber_clone/Screens/SearchPage.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final FirebaseApp app = await Firebase.initializeApp(
    name: 'db2',
    options: Platform.isIOS || Platform.isMacOS
        ? const FirebaseOptions(
            appId: '1:365424826617:android:78ca580aff50cbff9f2a28',
            apiKey: 'AIzaSyApwBAFrEI7qALgn0fLM4ODtxUVRBvHerY',
            projectId: 'instagram-clone-86a6e',
            messagingSenderId: '365424826617',
            databaseURL: 'https://instagram-clone-86a6e.firebaseio.com',
          )
        : const FirebaseOptions(
            appId: '1:365424826617:android:78ca580aff50cbff9f2a28',
            apiKey: 'AIzaSyApwBAFrEI7qALgn0fLM4ODtxUVRBvHerY',
            messagingSenderId: '365424826617',
            projectId: 'instagram-clone-86a6e',
            databaseURL: 'https://instagram-clone-86a6e.firebaseio.com',
          ),
  );

  runApp(MaterialApp(
    title: 'Flutter Database Example',
    home: MyApp(app: app),
  ));
}

class MyApp extends StatelessWidget {
  final FirebaseApp app;

  const MyApp({Key key, this.app}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    User currentFirebaseUser = FirebaseAuth.instance.currentUser;

    return ChangeNotifierProvider(
      create: (context) => AppData(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          fontFamily: 'Brand-Regular',
          primarySwatch: Colors.blue,
        ),
        home: currentFirebaseUser != null ? MainPage() : LoginPage(),
        routes: {
          RegistrationPage.id: (context) => RegistrationPage(),
          LoginPage.id: (context) => LoginPage(),
          MainPage.id: (context) => MainPage(),
          SearchPage.id: (context) => SearchPage()
        },
      ),
    );
  }
}
